import { Component, OnInit } from '@angular/core';
import { TodoService } from 'src/app/shared/todo.service';
import { Todo } from 'src/app/shared/todo.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/user.service';

@Component({
  selector: 'app-todo-lista',
  templateUrl: './todo-lista.component.html',
  styleUrls: ['./todo-lista.component.css']
})
export class TodoListaComponent implements OnInit {

  
  list: Todo[];
  users: any = [];

  constructor(private service : TodoService,
    private firestore: AngularFirestore,
    private toastr: ToastrService,
    protected userService: UserService) { }

  ngOnInit() {

    this.userService.getUsers()
    .subscribe(
      (data) => { // Success
        this.users = data;
      },
      (error) => {
        console.error(error);
      }
    );

    this.service.getTodos().subscribe(actionArray => {
      this.list = actionArray.map(item => {

        return {
          id: item.payload.doc.id,
          ...item.payload.doc.data()
        } as Todo;


      });
    });

  }

  onEdit(a:Todo){
    this.service.formData =Object.assign({},a);
  }

  onDelete(id: string){
    if(confirm("¿Está seguro que desea borrar la Tarea?")){
      this.firestore.doc('todos/' + id).delete();
      this.toastr.warning('Delete successfully','Registro');
    }

  }
}
