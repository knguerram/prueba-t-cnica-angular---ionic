import { Component, OnInit } from '@angular/core';
import { TodoService } from 'src/app/shared/todo.service'
import { NgForm } from "@angular/forms";
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  constructor(private service : TodoService,
    private firestore:AngularFirestore, 
    private toastr : ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form? : NgForm){

    if(form != null)
    form.resetForm();
    this.service.formData = {
      id: null,
      idTarea : '',
      title : ''
    }
  }

  onSumbit(form : NgForm){
    let data = Object.assign({}, form.value);
    delete data.id;
    console.log(form.value.id);
    if (form.value.id == null)
      this.firestore.collection('todos').add(data);
    else
      this.firestore.doc('todos/' + form.value.id).update(data);
    this.resetForm(form);
    this.toastr.success("Elemento enviado",'Se Guardo con exito!');
  }

}
