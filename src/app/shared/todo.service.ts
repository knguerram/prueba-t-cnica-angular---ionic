import { Injectable } from '@angular/core';
import { Todo } from './todo.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  formData : Todo;

  constructor( private firestore : AngularFirestore) {}

  getTodos(){
    return this.firestore.collection("todos").snapshotChanges();
  }
}
