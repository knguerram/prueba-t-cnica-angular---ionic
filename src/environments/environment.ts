// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC0XIzdLNbnvFHb3Yf_FqwF3LkoXw_LjI0",
    authDomain: "prueba-tecnica-d9f8b.firebaseapp.com",
    databaseURL: "https://prueba-tecnica-d9f8b.firebaseio.com",
    projectId: "prueba-tecnica-d9f8b",
    storageBucket: "prueba-tecnica-d9f8b.appspot.com",
    messagingSenderId: "831137584985",
    appId: "1:831137584985:web:85ee390db0b689c6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
